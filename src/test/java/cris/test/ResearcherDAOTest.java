package cris.test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import dao.ResearcherDAO;
import dao.ResearcherDAOImplementation;
import dataset.CSV2DB;
import model.Researcher;

class ResearcherDAOTest {
	  private Researcher r;
	  private ResearcherDAO rdao;
	 
	  @BeforeAll
	  static void dbSetUp() throws Exception {
	        CSV2DB.loadPublicationsFromCSV("publications.csv");
	        CSV2DB.loadResearchersFromCSV("researchers.csv");
	   }

	  @BeforeEach
	  void setUp() throws Exception {
	        rdao = ResearcherDAOImplementation.getInstance();
	        r = new Researcher();
	        r.setEmail("12345343100");
	        r.setId("12345343100");
	        r.setLastname("Ferreiros");
	        r.setName("Javier");
	        r.setPassword("1234");
	        r.setScopusURL("https://www.scopus.com/authid/detail.uri?authorId=12345343100");
	  }
	  
	  @Test
	  void testCreate() {
	          rdao.delete(r);
	          rdao.create(r);
	          assertEquals(r, rdao.read(r.getId()));
	  }

	  @Test
	  void testRead() {
	          assertEquals(r, rdao.read(r.getId()));
	  }

	  @Test
	  void testUpdate() {
	          String oldpwd = r.getPassword();
	          r.setPassword("1111");
	          rdao.update(r);
	          assertEquals(r, rdao.read(r.getId()));
	          r.setPassword(oldpwd);
	          rdao.update(r);
	  }

	  @Test
	  void testDelete() {
	          rdao.delete(r);
	          assertNull(rdao.read(r.getId()));
	          rdao.create(r);
	  }

	  @Test
	  void testReadAll() {
	          assertTrue(rdao.readAll().size() > 75);
	  }

	  @Test
	  void testReadByEmail() {
	          assertEquals(r, rdao.readByEmail(r.getEmail()));
	  }
}
