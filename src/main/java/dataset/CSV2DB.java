package dataset;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import com.itextpdf.io.IOException;

import dao.PublicationDAO;
import dao.PublicationDAOImplementation;
import dao.ResearcherDAO;
import dao.ResearcherDAOImplementation;
import model.Publication;
import model.Researcher;

public class CSV2DB {

        public static void main(String[] args) throws Exception {

                String f1 = args.length > 1 ? args[0] : "researchers.csv";
                String f2 = args.length > 2 ? args[1] : "publications.csv";
                CSV2DB.loadResearchersFromCSV(f1);
                CSV2DB.loadPublicationsFromCSV(f2);
        }
        
        public static void loadPublicationsFromCSV(String f2)
                        throws IOException, java.io.IOException {
                BufferedReader br;
                String line;
                final String EXPECTED_HEADER2 = "id,title,eid,publicationName,publicationDate,firstAuthor,authors";
                PublicationDAO pdao = PublicationDAOImplementation.getInstance();
                br = new BufferedReader(new InputStreamReader(new FileInputStream(new File(f2)), "UTF8"));
                line = br.readLine();
                int i = 0;
                if (EXPECTED_HEADER2.equals(line)) {
                        while (null != (line = br.readLine())) {
                                String[] s = line.split(",");
                                Publication p = new Publication();
                                p.setId(s[0]);
                                p.setTitle(s[1]);
                                p.setPublicationName(s[3]);
                                p.setPublicationDate(s[4]);
                                p.setAuthors(s[6]);
                                if (null == pdao.read(p.getId()))
                                        pdao.create(p);
                                i++;
                        }
                }
                br.close();
        }

        public static void loadResearchersFromCSV(String f1)
                        throws IOException, Exception {
                final String EXPECTED_HEADER1 = "id,name,lastName,scopusUrl,eid";
                final ResearcherDAO rdao = ResearcherDAOImplementation.getInstance();
                BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(new File(f1)), "UTF8"));
                String line = br.readLine();
                int i = 0;
                if (EXPECTED_HEADER1.equals(line)) {
                        while (null != (line = br.readLine())) {
                                String[] s = line.split(",");
                                Researcher r = new Researcher();
                                r.setId(s[0]);
                                r.setName(s[1]);
                                r.setLastname(s[2]);
                                r.setEmail(s[0]);
                                r.setScopusURL(s[3]);
                                r.setPassword("1234");
                                if (null == rdao.read(r.getId())) {
                                        rdao.create(r);
                                        i++;
                                        System.out.println(i);
                                }
                        }
                }
                br.close();
        }
}
