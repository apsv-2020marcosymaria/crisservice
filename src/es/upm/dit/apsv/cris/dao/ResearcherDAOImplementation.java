package es.upm.dit.apsv.cris.dao;

import java.util.List;

import org.hibernate.Session;

import es.upm.dit.apsv.cris.model.Researcher;

public class ResearcherDAOImplementation implements ResearcherDAO {

	private static ResearcherDAOImplementation instance = null; //solo puede haber un objeto en momeria
	private ResearcherDAOImplementation() {}
	public static ResearcherDAOImplementation getInstance() {
	   if( null == instance ) {
	       instance = new ResearcherDAOImplementation();
	   }
	   return instance;
	}
	
	@Override
	public Researcher create(Researcher researcher) {
		Session session = SessionFactoryService.get().openSession();
		try {
		  session.beginTransaction(); //transacción: operación con la BdD
		  session.save(researcher);
		  session.getTransaction().commit();//todo va a ir bien
		} catch (Exception e) {
		  // handle exceptions
		} finally {
		  session.close(); //cerrar la seción
		}
		return researcher;
	}

	@Override
	public Researcher read(String researcherId) {
		Session session = SessionFactoryService.get().openSession();
		Researcher r=null;
		try {
		  session.beginTransaction(); //transacción: operación con la BdD
		  r=session.get(Researcher.class,researcherId);
		  session.getTransaction().commit();//todo va a ir bien
		} catch (Exception e) {
		  // handle exceptions
		} finally {
		  session.close(); //cerrar la seción
		}
		return r;
	}

	@Override
	public Researcher update(Researcher researcher) {
		Session session = SessionFactoryService.get().openSession();
		try {
		  session.beginTransaction(); //transacción: operación con la BdD
		  session.saveOrUpdate(researcher);
		  session.getTransaction().commit();//todo va a ir bien
		} catch (Exception e) {
		  // handle exceptions
		} finally {
		  session.close(); //cerrar la seción
		}
		return researcher;
	}

	@Override
	public Researcher delete(Researcher researcher) {
		Session session = SessionFactoryService.get().openSession();
		try {
		  session.beginTransaction(); //transacción: operación con la BdD
		  session.delete(researcher);
		  session.getTransaction().commit();//todo va a ir bien
		} catch (Exception e) {
		  // handle exceptions
		} finally {
		  session.close(); //cerrar la seción
		}
		return researcher;
	}

	@Override
	public List<Researcher> readAll() {
		Session session = SessionFactoryService.get().openSession();
			List<Researcher> list=null;
		try {
		  session.beginTransaction(); //transacción: operación con la BdD
		  list=(List<Researcher>)session.createQuery("from Researcher").list();
		  session.getTransaction().commit();//todo va a ir bien
		} catch (Exception e) {
		  // handle exceptions
		} finally {
		  session.close(); //cerrar la seción
		}
		return list;
	}

	@Override
	public Researcher readByEmail(String email) {
		for (Researcher r :this.readAll())
			if(email.equals(r.getEmail()))//si el correo que me dan es igual que el de algun investigador guardado
				return r;
		return null;
	}

}
