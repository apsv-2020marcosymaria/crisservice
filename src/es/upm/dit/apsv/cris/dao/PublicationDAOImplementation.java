package es.upm.dit.apsv.cris.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;

import es.upm.dit.apsv.cris.model.Publication;
import es.upm.dit.apsv.cris.model.Researcher;

public class PublicationDAOImplementation implements PublicationDAO {
	private static PublicationDAOImplementation instance = null;
	private PublicationDAOImplementation() {}
	public static PublicationDAOImplementation getInstance() {
	   if( null == instance ) {
	       instance = new PublicationDAOImplementation();
	   }
	   return instance;
	}
	@Override
	public Publication create(Publication publication) {
		Session session = SessionFactoryService.get().openSession();
		try {
		  session.beginTransaction(); //transacción: operación con la BdD
		  session.save(publication);
		  session.getTransaction().commit();//todo va a ir bien
		} catch (Exception e) {
		  // handle exceptions
		} finally {
		  session.close(); //cerrar la seción
		}
		return publication;
	}

	@Override
	public Publication read(String publicationId) {
		Session session = SessionFactoryService.get().openSession();
		Publication p=null;
		try {
		  session.beginTransaction(); //transacción: operación con la BdD
		  p=session.get(Publication.class,publicationId);
		  session.getTransaction().commit();//todo va a ir bien
		} catch (Exception e) {
		  // handle exceptions
		} finally {
		  session.close(); //cerrar la seción
		}
		return p;
	}

	@Override
	public Publication update(Publication publication) {
		Session session = SessionFactoryService.get().openSession();
		try {
		  session.beginTransaction(); //transacción: operación con la BdD
		  session.saveOrUpdate(publication);
		  session.getTransaction().commit();//todo va a ir bien
		} catch (Exception e) {
		  // handle exceptions
		} finally {
		  session.close(); //cerrar la seción
		}
		return publication;
	}

	@Override
	public Publication delete(Publication publication) {
		Session session = SessionFactoryService.get().openSession();
		try {
		  session.beginTransaction(); //transacción: operación con la BdD
		  session.delete(publication);
		  session.getTransaction().commit();//todo va a ir bien
		} catch (Exception e) {
		  // handle exceptions
		} finally {
		  session.close(); //cerrar la seción
		}
		return publication;
	}

	@Override
	public List<Publication> readAll() {
		Session session = SessionFactoryService.get().openSession();
		List<Publication> list=null;
	try {
	  session.beginTransaction(); //transacción: operación con la BdD
	  list=(List<Publication>)session.createQuery("from Publication").list();
	  session.getTransaction().commit();//todo va a ir bien
	} catch (Exception e) {
	  // handle exceptions
	} finally {
	  session.close(); //cerrar la seción
	}
	return list;
	}

	@Override
	public List<Publication> readAllPublications(String researcherId) {
		//indexOf devuelve posicion en la que se encuentra researcherId dentro de getAuthors, si no esta devuelve -1
		List<Publication> list=new ArrayList<Publication>();
		for (Publication p :this.readAll())
			if(p.getAuthors().indexOf(researcherId)> -1)//si el correo que me dan es igual que el de algun investigador guardado
				list.add(p);
		return list;
	}
	
}
